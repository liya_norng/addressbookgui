import static org.junit.Assert.*;

import org.junit.Test;

public class AddressBookTest {

	@Test
	public void testNewEntry() {
			AddressBook contact = new AddressBook();
			contact.newEntry ("1234", "Liya", "Norng", "311 B street", "Tracy","CA.","95376", "Liya_Norng@yahoo.com", "(209) 346-2998");
			assertEquals(contact.count("Norng"), 1);
			contact.newEntry("312", "Kado", "Norng", "311 B street", "Tracy","CA.","95376", "kadonorng@yahoo.com", "(209) 346-2998");
			assertEquals(contact.count("Norng"), 1);
			contact.newEntry("2345", "Kanal", "Norng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
			assertEquals(contact.count("Norng"), 1);
	}

	@Test
	public void testCheck() 
	{
		AddressBook contact = new AddressBook();
		AddressEntry entry = new AddressEntry();
		Address address = new Address();
		Name name = new Name();
		contact.newEntry ("1234", "Liya", "Norng", "311 B street", "Tracy","CA.","95376", "Liya_Norng@yahoo.com", "(209) 346-2998");
		entry = contact.setInfo("1234");
		address = entry.getAddress();
		name = entry.getName();
		assertEquals(contact.check("Norng"), entry.toString(address, name));		
		contact.newEntry("2345", "Kanal", "Norng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		entry = contact.setInfo("2345");
		address = entry.getAddress();
		name = entry.getName();
		assertEquals(contact.check("Norng"), entry.toString(address, name));	
	}

	@Test
	public void testGetInfo()
	{
		// similar to testcheck()
	}

	@Test
	public void testGetInfoForCorrection() 
	{
		// this is also the same as the previous one
	}

	@Test
	public void testSetInfo() {
		// i call this method early to set the contact
	}

	@Test
	public void testCount() {
		AddressBook contact = new AddressBook();
		contact.newEntry("123","Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		assertEquals(1,contact.count("Seng"));	
		contact.newEntry("321" , "Kanal", "Norng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		assertEquals(1,contact.count("Norng"));
	}
}
