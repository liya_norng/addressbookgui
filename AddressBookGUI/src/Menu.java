/**
 * 
 * @author Liya Norng
 * @version 1.0
 * @since 1/20/2016
 *
 */

public class Menu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * 
	 * @return the prompt asking for first name
	 */
	public String FirstName()
	{
		return ("First Name: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for last name
	 */
	public String LastName()
	{
		return ("Last Name: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for street
	 */
	public String Street()
	{
		return ("Street: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for city
	 */
	public String City()
	{
		return ("City: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for state
	 */
	public String State()
	{
		return ("State: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for zip
	 */
	public String Zip()
	{
		return ("Zip: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for phone number
	 */
	public String Telephone()
	{
		return ("Telephone: ");
	}
	
	/**
	 * 
	 * @return the prompt asking for email address
	 */
	public String Email()
	{
		return ("Email: ");
	}

}
