
/**
 * @author Liya Norng
 * @date 2/17/16
 */


/**
 * @param first_Name is first name for storing the information.
 * @param last_Name is last name for storing the information. *
 */

public class Name
{
	// this is my variables for class name
	String first_Name;
    String last_Name;

    /**
     * all of this methods would set all the string to appropriate string.
     */
    public void setFirstName(String first_Name)
    {
    	this.first_Name = first_Name;
    }
    public void setLastName(String last_Name)
    {
    	this.last_Name = last_Name;
    }

    /**
     * @return first_Name this value is to return to the request of the class.
     * @return last_Name this value is to return to the request of the class.
     */
    public String getFirstName()
    {
    	return first_Name;
    }
    public String getLastName()
    {
    	return last_Name;
    }
}