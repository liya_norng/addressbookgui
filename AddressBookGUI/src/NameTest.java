import static org.junit.Assert.*;

import org.junit.Test;

public class NameTest {

	@Test
	public void testSetFirstName() {
		Name contact = new Name();
		contact.setFirstName("Liya");
		assertEquals("Liya", contact.getFirstName());
		contact.setFirstName("Kado");
		assertEquals("Kado", contact.getFirstName());	
		}

	@Test
	public void testSetLastName() {
		Name contact = new Name();
		contact.setLastName("Norng");
		assertEquals("Norng", contact.getLastName());
		contact.setLastName("Seng");
		assertEquals("Seng", contact.getLastName());	
		}
}
