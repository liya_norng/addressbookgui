import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.Highlighter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

/**
 * @author Liya Norng
 * @date 2/17/16
 */

public class Dialog {

	private JFrame frame2;
	static AddressBook contact = new AddressBook();
	String first_Name;
	String last_Name;
	String street_Name;
    String city_Name;
    String state_Name;
    String zip_Name;
    String email_Name;
    String phone_Name;
    private JTextField textField_8;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTextField textField_6;
    private JTextField textField_7;



	/**
	 * Launch the application.
	 */
	public static void SecondWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dialog window = new Dialog();
					window.frame2.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Dialog() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		/**
		 * @param lblPleaseEnterIn is label for telling user to enter the contact
		 * @param lblFirstName is the label for telling the user first name
		 * @param lblLastName is the label for telling the user last name
		 * @param lblStreet is the label for telling the user street
		 * @param lblCity is the label for telling the user city
		 * @param lblState is the label for telling the user state
		 * @param lblZipCode is the label for telling the user zip code
		 * @param lblEmailAddress is the label for telling the user email address
		 * @param lblPhoneNumber is the label for telling the user phone number
		 * @param btnCanel is the button for closing the frame and return back to the main frame with out adding the contact
		 * @param btnEnter is the button for adding the contact to the map and close the frame
		 * @param textField is the field that display the current matching contact
		 */
		frame2 = new JFrame("Adding New Contact");
		frame2.setBounds(100, 100, 450, 300);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel lblPleaseEnterIn = new JLabel("Please Enter in Your Contact");
		JLabel lblFirstName = new JLabel("First Name");
		JLabel lblLastName = new JLabel("Last Name");
		JLabel lblStreet = new JLabel("Street");
		JLabel lblCity = new JLabel("City");
		JLabel lblState = new JLabel("State");
		JLabel lblZipCode = new JLabel("Zip code");
		JLabel lblEmailAddress = new JLabel("Email Address");
		JLabel lblPhoneNumber = new JLabel("Phone number");
		JButton btnCancel = new JButton("Cancel");
		JButton btnEnter = new JButton("Enter");
		JTextField textField = new JTextField();
		
		// creating an instance of the classes
		textField_8 = new JTextField();
		textField_1 = new JTextField();
		textField_2 = new JTextField();
		textField_3 = new JTextField();
		textField_4 = new JTextField();
		textField_5 = new JTextField();
		textField_6 = new JTextField();
		textField_7 = new JTextField();

		/**
		 * This is keyListenter for getting the first name from the user
		 */
		textField_8.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				if (textField_8.getText().equals("*"))
				{
					textField_8.setText("");
				}
				first_Name = textField_8.getText();		
			}
		});
		textField_8.setColumns(10);
		
		/**
		 * This is keyListenter for getting the street name from the user
		 */
		textField_1.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				street_Name = textField_1.getText();
			}
		});
		textField_1.setColumns(10);
		
		/**
		 * This is keyListenter for getting the city name from the user
		 */
		textField_2.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				city_Name = textField_2.getText();
			}
		});
		textField_2.setColumns(10);
		
		/**
		 * This is keyListenter for getting the state name from the user
		 */
		textField_3.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				state_Name = textField_3.getText();
			}
		});
		textField_3.setColumns(10);
		
		/**
		 * This is keyListenter for getting the last name from the user
		 */
		textField_4.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				if (textField_4.getText().equals("*"))
				{
					textField_4.setText("");
				}
				last_Name = textField_4.getText();
			}
		});
		textField_4.setColumns(10);
		
		/**
		 * This is keyListenter for getting the email from the user
		 */
		textField_5.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e)
			{
				if (textField_5.getText().equals("*"))
				{
					textField_5.setText("");
				}
				
				email_Name = textField_5.getText();
			}
		});
		textField_5.setColumns(10);
		
		/**
		 * This is keyListenter for getting the phone number from the user
		 */
		textField_6.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e) 
			{
				phone_Name = textField_6.getText();				
			}
		});
		textField_6.setColumns(10);
		
		/**
		 * This is keyListenter for getting the zip code from the user
		 */
		textField_7.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e)
			{
				zip_Name = textField_7.getText();
			}
		});
		textField_7.setColumns(10);
		
		/**
		 * This is actionListenter closing the current window gui and add nothing to the map
		 */
		btnCancel.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// do an exit out the window 
				// without saving the contact
				textField.setText("You did not add any new contact.");
				frame2.dispose();
			}
		});
		
		/**
		 * This is actionListenter closing the current window gui and add the new entry to the map
		 */
		btnEnter.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// so this is where i do a check if the user didn't give me the information. To me this is important when filling out any document
				// So if the user didn't give me anything on the location it will never let the user to add the entry to the contact
				if (first_Name != null && email_Name != null && last_Name != null)
				{
					// get the size from the current array so i can know how many there are so i can add to the randomize number to create a ID number
					int size = contact.getSize();
					Random rand = new Random();
					int value = rand.nextInt(100) + 20;
					size = size + value;
					String id = String.valueOf(size);
					// setting new entry by calling to the addressbook
					contact.newEntry(id, first_Name, last_Name, street_Name, city_Name, state_Name, zip_Name, email_Name, phone_Name);
					AddressBookGUI gui = new AddressBookGUI();
					textField.setText("You have added the new contact successfully.");
					gui.setNewKey(id);
					// do an exit out the window
					frame2.dispose();			
				}
				
				// this is for settint the requirement part if the user didn't put anything in the slot
				textField_8.setText("*");
				textField_5.setText("*");	
				textField_4.setText("*");
				first_Name = null;
				last_Name = null;
			    email_Name = null;
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(frame2.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(32)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCity)
						.addComponent(lblStreet)
						.addComponent(textField_8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblFirstName)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblState)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCancel))
					.addPreferredGap(ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLastName)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblEmailAddress, Alignment.LEADING)
							.addComponent(textField_5, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblPhoneNumber, Alignment.LEADING)
							.addComponent(lblZipCode, Alignment.LEADING))
						.addComponent(textField_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnEnter)
						.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(87, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(97)
					.addComponent(lblPleaseEnterIn)
					.addContainerGap(176, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblPleaseEnterIn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLastName)
						.addComponent(lblFirstName))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblStreet)
						.addComponent(lblEmailAddress))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblCity)
						.addComponent(lblPhoneNumber))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblZipCode)
						.addComponent(lblState, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(textField_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnCancel)
						.addComponent(btnEnter))
					.addGap(35))
		);
		frame2.getContentPane().setLayout(groupLayout);
	}
}
