import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.accessibility.AccessibleContext;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.JTextArea;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JToggleButton;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


/**
 * @author Liya Norng
 * @date 2/17/16
 */

public class AddressBookGUI
{

	/**
	 * @param frame is the frame of the window gui, it is the main window
	 * @param model is the list model that add into the jlist
	 * @param currentContact is an array that contains the current contact
	 * @param newContact is an array that contains the new contact
	 * @param removeContact is an array that contains the remove contact from the data base
	 */
	private JFrame frame;
	DefaultListModel model = new DefaultListModel();
    private static ArrayList <String> currentContact = new ArrayList<>();
    private  static ArrayList <String> newContact = new ArrayList<>();
    private  static ArrayList <String> removeContact = new ArrayList<>();
    
    /**
     * @param contact is an instance of AddressBook class
     * @param entry is an instance of AddressEntry class
     * @param value is an int that the user select from the jlist. 
     * @param textField is a field to display a text. In this case it display the current situation of the action
     */
    AddressBook contact = new AddressBook();
    AddressEntry entry = new AddressEntry();
	static int value;
	private JTextField textField;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddressBookGUI window = new AddressBookGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddressBookGUI() 
	{
		initialize();
	}
	
	/**
	 * 
	 * @param key is a string. It is the key to access the map
	 * So in this it add the key to the newContact array to keep track later for adding into the data base
	 */
	void setNewKey(String key)
	{
		newContact.add(key);
	}
	
	/**
	 * 
	 * @param key is a string. It is the key to access the map
	 * So in this it add the key to the removeContact array to keep track later for removing the contact from the data base
	 */
	void setRemoveKey(String key)
	{
		removeContact.add(key);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		/**
		 * this is all the gui stuffs that create a button for remove, search, save&exit, done, load, display, and new.
		 * @param btnRemove is the button for removing
		 * @param frame is the main window on the gui
		 * @param lblListOfContacts is the label for defining what the two text display were
		 * @param lblContactEntry is the label for defining what the two text display were
		 * @param scrollPane is the scroll bar for scrolling in the jlist
		 * @param btnSearch is the button for searching
		 * @param btnSE is the button for save and exit
		 * @param btnDone is the button for detecting if user is done editing the entry
		 * @param btnLoad is the button for loading the contacts in the data base
		 * @param list is the jlist that i keep the list of the contacts
		 * @param btnDisplay is the button for display the contacts in the currentContact array
		 * @param btnNew is the button for adding a new entry to the contact
		 */
		frame = new JFrame("Address Book GUI Application");
		JLabel lblListOfContacts = new JLabel("List of Contacts");
		JLabel lblContactEntry = new JLabel("Contact Entry");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JScrollPane scrollPane = new JScrollPane();
		JButton btnRemove = new JButton("Remove");
		JButton btnSearch = new JButton("Search");
		JButton btnSE = new JButton("Save&Exit");
		JButton btnDone = new JButton("Done");
		JButton btnLoad = new JButton("Load");
		JList list = new JList(model);
		JButton btnDisplay = new JButton("Display");
		JButton btnNew = new JButton("New");
		textField = new JTextField();
		textField.setColumns(10);
		JTextArea textArea = new JTextArea();
		JScrollPane scrollPane_1 = new JScrollPane();
		
		/**
		 * this is where I set the visibility to the user. For example if the contact is not in the array. It will not allow 
		 * user to select display, or remove ect.
		 */
		btnDisplay.setEnabled(false);
		btnRemove.setEnabled(false);
		btnSearch.setEnabled(false);
		btnSE.setEnabled(false);
		btnDone.setEnabled(false);

		/**
		 * this is where the Done button actionListener happen. This is for checking if the user is done editting the contact entry. 
		 * So if the user is done, the user have to click on the button done to reset the infomation
		 */
		btnDone.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				/**
				 * @param lines is an array of the string from the contact entry
				 * @param i is the counting loop
				 * @param entry is an instance of class AddressEntry
				 * @param address is an instance of class Name
				 * 
				 * so it will read from the entry, and store it into the array. 
				 * From there i did a while loop and set the infomation to the correct spot
				 */
				String[] lines = null;
				lines = textArea.getText().split("\\n"); 
				int i = 0;
				AddressEntry entry = contact.setInfo(currentContact.get(value));
				Address  address = entry.getAddress();
				Name name = entry.getName();
				while (i < 23)
				{
					if ( i == 1)
					{
						name.setFirstName(lines[i]);
					}
					else if (i == 4)
					{
						name.setLastName(lines[i]);
					}
					else if (i == 7)
					{
						address.setStreet(lines[i]);
					}
					else if (i == 10)
					{
						address.setCity(lines[i]);
					}
					else if (i == 13)
					{
						address.setState(lines[i]);
					}
					else if (i == 16)
					{
						address.setZip(lines[i]);
					}
					else if (i == 19)
					{
						entry.setEmail(lines[i]);
					}
					else if (i == 22)
					{
						entry.setPhone(lines[i]);
					}
					i++;
				}
			}
		});

		/**
		 * this is my action listener for selecting which contact to remove.
		 */
		list.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
				// this if statement is for checking if the list is not empty, then it will allow user to remove. That is saying
				// the user have select a contact from the model.
				if (!list.isSelectionEmpty())
				{
					btnDone.setEnabled(true);
					btnRemove.setEnabled(true);
					value = list.getSelectedIndex();
					textArea.setText(contact.getInfoForCorrection(currentContact.get(value)));
				}
				else
				{
					textArea.setText("");
					btnRemove.setEnabled(false);
					btnDone.setEnabled(false);
				}
				// if the model is empty, then it can't be save and exit,  and search.
				if (model.isEmpty())
				{
					btnSE.setEnabled(false);
					btnSearch.setEnabled(false);
				}
			}
		});
		
		/**
		 * this is my actionlistenter for my display button.
		 * it will set the array from my addressbook class to the array in this class. then
		 * from there I know what is in the array or should I said the ID that read from the data base
		 * so then i have a settext field that will display amount of contact.
		 * I have a for loop to get the ID from the array, and from there it'll go to the addressBook to get the contact.
		 * I also have if statement use for checking if the model is empty. if it is it will not allow user to search and exit and save
		 */
		btnDisplay.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// clearing the model everytime it calls. 
				// here i did a call addressbook to get the current key to the map. From there i can do a loop to access the map from the addressbook
				// Then i add the info to the model 
				// the if statement is to checking if there is any contacts so i can set the visibility.
				model.removeAllElements();
				currentContact = contact.ID();
				textField.setText("There are total of " + currentContact.size() + " contact.");
				for (int i = 0; i < currentContact.size(); i++)
				{
					model.addElement(contact.getInfo(currentContact.get(i)));
				}
				if (!model.isEmpty())
				{
					btnSearch.setEnabled(true);
					btnSE.setEnabled(true);
				}
				else
				{
					btnSearch.setEnabled(false);
				}	
			}
		});
		
		/**
		 * this is my actionlistenter for my button new. it will call to Dialog class to open another window and create a whole new contact.
		 */
		btnNew.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// here i create an instance of class dialog
				// to create another window for new contact
				btnSE.setEnabled(true);
				Dialog call = new Dialog();
				call.SecondWindow();
				btnDisplay.setEnabled(true);
			}
		});		
		
		/**
		 * this is my actionlistener for button remove. It will call to the addressbook and remove the key from the array. So then, i can 
		 * not access to that contact from the map. from there i have the value to remove from the model. Then, i set a text to say that the 
		 * user have remove
		 */
		btnRemove.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// Do a call to the addressBook to remove
				contact.remove(value);
				model.remove(value);
				textField.setText("You have remove the contact successfully.");
			}
		});
		
		/**
		 * this is my actionlistener for button search.
		 * it willl reset the testfield that tell the user the current situation. 
		 * It will call to another class to create another window for search.
		 * 
		 */
		btnSearch.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// it will create another window for searching for certain contact in the list
				textField.setText("");
				AddressBookSearch lookUp = new AddressBookSearch();
				lookUp.ThirdWindow();
			}
		});
		
		/**
		 * this is my actionListenter for save and exit
		 * this will save the data to the data base and exit
		 */
		btnSE.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// do a push and commit to the DB for the data that is in the Collection
				textField.setText("Thank you, your contact info have been save successfully.");
				AddressBookGUI a = new AddressBookGUI();
				a.getDB(1);
				frame.dispose();

			}
		});
		
		/**
		 * this is my actionListener for loading from the data base
		 */
		btnLoad.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// this is an instance of class addressbookgui so i can to the method for loading from data base
				AddressBookGUI a = new AddressBookGUI();
				btnDisplay.setEnabled(true);
				a.getDB(0);
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnDisplay, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNew, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnRemove, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSE, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnLoad))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblListOfContacts)
									.addGap(138))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
									.addComponent(btnDone, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblContactEntry)
									.addGap(19))
								.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)))
						.addComponent(textField, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDisplay)
						.addComponent(btnNew)
						.addComponent(btnRemove)
						.addComponent(btnSearch)
						.addComponent(btnSE)
						.addComponent(btnLoad))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblContactEntry)
						.addComponent(lblListOfContacts)
						.addComponent(btnDone))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE)
						.addComponent(scrollPane_1)))
		);
		
		// adding the list my scrollPane so i can scroll when i select. So it act as a container.
		scrollPane.setViewportView(list);
		scrollPane_1.setViewportView(textArea);
		frame.getContentPane().setLayout(groupLayout);
	}
	
	/**
	 * this method is to read from the data base. Also to remove, and insert to the data base.
	 */
	public void getDB(int version)
	{
		// this is the variables name that i store it for temp. until i set all the information in correct spot
		String first_Name = null;
		String last_Name = null;
		String street_Name = null;
		String city_Name = null;
		String state_Name = null;
		String zip_Name = null;
		String email_Name = null;
		String phone_Name = null;
		String id_Name = null;
		
		// so if the version is 0, it will read from the data base
		if (version == 0)
		{
			// so it will try to read from the data base
			try 
			{
				// this is the connection to thee data base
				Connection conn = 
				DriverManager.getConnection("jdbc:oracle:thin:uh8425/MozekTk2@mcsdb1.sci.csueastbay.edu:1521/MCSDB1");
				
				// Create a Statement
				Statement stmt = conn.createStatement ();
				
				// Select the all (*) from the table JAVATEST					
				ResultSet rset = stmt.executeQuery("SELECT * FROM ADDRESSENTRYTABLE");
				while (rset.next ()) //get next row of table returned
				{   
					// getting from the data base and setting into the correct name
					first_Name = rset.getString("First Name");
					last_Name = rset.getString("Last Name");
					street_Name = rset.getString("Street");
					city_Name = rset.getString("City");
					state_Name = rset.getString("State");
					zip_Name = rset.getString("Zip");
					email_Name = rset.getString("Email");
					phone_Name = rset.getString("Phone Number");
					id_Name = rset.getString("ID");
					// adding the new entry to the map
					contact.newEntry(id_Name, first_Name, last_Name, street_Name, city_Name, state_Name, zip_Name, email_Name, phone_Name);
				}
				
				// closing the connection
				rset.close();
				stmt.close();
				conn.close();		
			}		
			catch (SQLException e)
			{
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		}
		else
		{
			try 
			{
				// creating the connection to the data base
				Connection conn = 
				DriverManager.getConnection("jdbc:oracle:thin:uh8425/MozekTk2@mcsdb1.sci.csueastbay.edu:1521/MCSDB1");
				// create a Statement for the connection
				Statement statement;
				statement = conn.createStatement();
				int	i = 0;
				// a while loop to get the contact set it to my temp. variable and from there i can call to the data base and update the table
				// so this is for adding to the data base
				while (i < newContact.size())
				{
					// calling to the addressentry to get the entry of the contact
					// also calling to the address, and name classes
					AddressEntry info = contact.setInfo(newContact.get(i));
					Address address = info.getAddress();
					Name name = info.getName();
					first_Name = name.getFirstName();
					last_Name = name.getLastName();
					street_Name = address.getStreet();
					city_Name = address.getCity();
					state_Name = address.getState();
					zip_Name = address.getZip();
					email_Name = info.getEmail();
					phone_Name = info.getPhone();
					id_Name = info.getID();
					i++;
					String sqlcreate = "INSERT INTO ADDRESSENTRYTABLE "
										+ "VALUES ('" + first_Name + "','" + last_Name + "','" + street_Name + "','"
										+ city_Name + "','" + state_Name + "','" + zip_Name + "','" + email_Name + "','"
										+ phone_Name + "','" + id_Name +"')";
					// insert the data
					statement.executeUpdate(sqlcreate);
				}
				
				// this is for removing from the data base
				int j = 0;
				while (j < removeContact.size())
				{
					// this is an instance of class addressentry so i can access to get the ID so i can remove from the table from the data base
					AddressEntry info = contact.setInfo(removeContact.get(j));
					id_Name = info.getID();
					j++;
					String sqlcreate = "DELETE FROM ADDRESSENTRYTABLE WHERE ID =" + "'" + id_Name + "'";
					// updating the table by removing the correct ID
					statement.executeUpdate(sqlcreate);
				}
				// closing the connection for both removing and insert
				conn.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}


		
