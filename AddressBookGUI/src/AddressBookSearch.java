import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

/**
 * @author Liya Norng
 * @date 2/17/16
 */

public class AddressBookSearch {

	private JFrame frame1;
	String ID = null;
	ArrayList<String> array = new ArrayList<String>();
	DefaultListModel model = new DefaultListModel();
	AddressBook contact = new AddressBook();
	int value;

	private JTextField textField;
	/**
	 * Launch the application.
	 */
	public static void ThirdWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddressBookSearch window = new AddressBookSearch();
					window.frame1.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddressBookSearch() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		/**
		 * @param frame1 is an instance of class Jframe for the third window for searching the contact
		 */
		frame1 = new JFrame("Search for Contact");
		frame1.setBounds(100, 100, 450, 300);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/**
		 * @param lblPleaseEnterThe is a label to let user know that to enter the last name to search for contact
		 * @param scrollPane is an instance of class JScrollPane for scrolling through contact if the contact is fill up the JList
		 * @param list is the like a container for keeping all the list of the contact
		 * @param btnExit is a button for exiting current window.
		 */
		JLabel lblPleaseEnterThe = new JLabel("Please enter in the last name you would like to seach");
		JScrollPane scrollPane = new JScrollPane();
		JList list = new JList(model);
		textField = new JTextField();
		JButton btnExit = new JButton("Exit");
		scrollPane.setViewportView(list);
		
		/**
		 * this is my keyListener for detecting if the user is done typing for name to be searching.
		 */
		textField.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyReleased(KeyEvent e)
			{
				// i create an instance of class addressbook
				// i get the name from the textfield and set it to a string ID
				AddressBook contact = new AddressBook();
				ID = textField.getText();
				// i call to the addressbook to do a count and to store the key to the nums array
				int count = contact.count(ID);	

				// doing if statement if the it matches the contact ID, it will then execute this code
				if (count == 1)
				{
					// i clear what ever is in the model which is adding the JList
					// i call to the addressbook to get the current array which is ID array
					model.clear();
					array = contact.getNumArray();
					// so i did if statement if it is 0, it will tell the user that there is no name were found
					if (array.size() != 0)
					{
						// looping through the array and getting dislplaying the contact to the JList
						for (int i = 0; i < array.size(); i++)
						{
							if (i == 0)
							{
								model.clear();
							}
							model.addElement((contact.getInfo(array.get(i))));
							if (i == array.size())
							{
								array.clear();
							}
						}
					}
				}
				else
				{
					model.clear();
					model.addElement("Sorry, there was no contact were found by your request.");
				}
			}
			
			/**
			 * this is my key typed to detect if the user is still typing if it is it will erase everthing and start all over again
			 */
			@Override
			public void keyTyped(KeyEvent e) 
			{
				model.clear();
				array.clear();
			}
		});
		textField.setColumns(10);
		
		/**
		 * this is my actionListener for exiting out of the current window. It is the button exit. 
		 */
		btnExit.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// closing the current window 
				frame1.dispose();
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(frame1.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(45)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPleaseEnterThe, GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(textField, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)))
							.addGap(300))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblPleaseEnterThe)
					.addGap(2)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnExit))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
		);
		frame1.getContentPane().setLayout(groupLayout);
	}
}
