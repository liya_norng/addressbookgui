

/**
 * @author Liya Norng
 * @date 2/17/16
 */

/**
 * @param street_Name is street name for storing the information.
 * @param city_Name is city name for storing the information.
 * @param state_Name is state name for storing the information.
 * @param zip_Name is the zip code for storing the information. *
 */
public class Address
{
	// this is my variables for class address
 	String street_Name;
 	String city_Name;
 	String state_Name;
 	String zip_Name;
	 
	/**
     * all of this methods would set all the string to appropriate string.	
     */
 	public void setStreet(String street_Name)
  	{
	 	this.street_Name = street_Name;
    }
 	public void setCity(String city_Name)
 	{
 		this.city_Name = city_Name;
 	}
	public void setState(String state_Name)
	{
		this.state_Name = state_Name;
	}
	public void setZip(String zip_Name)
	{
	    this.zip_Name = zip_Name;
	}
	    
	/**
	 * @return street_Name this value is to return to the request of the class.
	 * @return city_Name this value is to return to the request of the class.
	 * @return state_Name this value is to return to the request of the class.
	 * @return zip_Name this value is to return to the request of the class.
	 */
	public String getStreet()
	{
		return street_Name;
	}
	public String getCity()
	{
		return city_Name;
	}
	public String getState()
	{
		return state_Name;
	}
	public String getZip()
	{
		return zip_Name;
	} 
}



