


import static org.junit.Assert.*;

import org.junit.Test;

public class AddressEntryTest
{
	@Test
	public void testAddressEntryStringStringStringStringStringStringStringStringString() {
		AddressEntry contact = new AddressEntry("234", "Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		Address address = contact.getAddress();
		Name name = contact.getName();
		String s =  ("Kanal" + "\n" +  "Seng" + "\n" + "123 A Street" + "\n" + "Tracy" + "\n" + "CA." +  "\n" + "95376" + "\n" + "Kanalnorng@yahoo.com" + "\n" + "(209) 346-2998");
		assertEquals(contact.toStringForCorrection(address, name), s);	}

	@Test
	public void testAddressEntry() {
		// dummy constructor
	}

	@Test
	public void testGetAddress() 
	{
		AddressEntry contact = new AddressEntry();
		Address address = contact.getAddress();
		address.setStreet("311 B Street");
		assertEquals("311 B Street", address.getStreet());
		address.setStreet("588 B Street");
		assertEquals("588 B Street", address.getStreet());
		address.setCity("Tracy");
		assertEquals("Tracy", address.getCity());
		address.setCity("Hayward");
		assertEquals("Hayward", address.getCity());	
		address.setState("Ca");
		assertEquals("Ca", address.getState());
		address.setState("Tx");
		assertEquals("Tx", address.getState());
		address.setZip("12345");
		assertEquals("12345", address.getZip());
		address.setZip("09876");
		assertEquals("09876", address.getZip());
	}

	@Test
	public void testGetName()
	{
		AddressEntry contact = new AddressEntry();
		Name name = contact.getName();
		name.setLastName("Norng");
		assertEquals("Norng", name.getLastName());
		name.setLastName("Seng");
		assertEquals("Seng", name.getLastName());
		name.setFirstName("Liya");
		assertEquals("Liya", name.getFirstName());
		name.setFirstName("Kado");
		assertEquals("Kado", name.getFirstName());	
	}


	@Test
	public void testSetID() {
		AddressEntry contact = new AddressEntry();
		contact.setID("1234");
		assertEquals("1234", contact.getID());
	}

	@Test
	public void testSetEmail() {
		AddressEntry contact = new AddressEntry();
		contact.setEmail("Liya_Norng@yahoo.com");
		assertEquals("Liya_Norng@yahoo.com", contact.getEmail());
		contact.setEmail("Kado_Norng@yahoo.com");
		assertEquals("Kado_Norng@yahoo.com", contact.getEmail());		
		}

	@Test
	public void testSetPhone() {
		AddressEntry contact = new AddressEntry();
		contact.setPhone("209-234-2222");
		assertEquals("209-234-2222", contact.getPhone());
		contact.setPhone("334-343-2342");
		assertEquals("334-343-2342", contact.getPhone());	
		}
}
