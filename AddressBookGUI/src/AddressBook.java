import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Liya Norng
 * @date 2/17/16
 */

/**
 * this is the AddressBook class
 * it take the AddressEntry and put it in the
 * map
 */
public class AddressBook {
    /**
     * @param addressEntryTable is all the entry of AddressEntry with the key.
     * @param nums is an array for keeping the key for the map. It is for checking if the name is matches when searching
     * @param ID is an array of current contacts in the list
     */
    private static Map<String,AddressEntry> addressEntryTable = new HashMap<String, AddressEntry>();
    private static ArrayList<String> nums = new ArrayList<String>();
    private static ArrayList<String> ID = new ArrayList<String>();    
    
    /**
     * getting all the information about the person
     * @param id_Name
     * @param first_Name
     * @param last_Name
     * @param street_Name
     * @param city_Name
     * @param state_Name
     * @param zip_Name
     * @param email_Name
     * @param phone_Name
     * @param contact is the instance of AddressEntry class
     * @param key_Name is string for the key to access all the contacts
     * adding all the key name and the contacts to the map.
     */
    public void newEntry(String id_Name, String first_Name, String last_Name, String street_Name, String city_Name, String state_Name, String zip_Name, String email_Name, String phone_Name)
    {    	
    	// adding all the info of one person at a time to the addressentry, and from there i call map to add in the addressentry
        AddressEntry contact = new AddressEntry(id_Name, first_Name, last_Name, street_Name, city_Name, state_Name, zip_Name, email_Name, phone_Name);
        ID.add(id_Name);
        addressEntryTable.put(id_Name, contact);
    }
    
    /**
     * 
     * @param name is the last name for checking in the map
     * @return return a string of entries
     */
    public String check(String name)
    {
    	String call = "";
    	for (String key: addressEntryTable.keySet())
        {
    		AddressEntry info_List = addressEntryTable.get(key);
    		Name lastName = info_List.getName();
    		Address address = info_List.getAddress();
    		if (lastName.getLastName().equals(name))
    		{
    	        call = info_List.toString(address, lastName);
    		}
    		else
    		{
    			call = null;
    		}
        }
		return call;
    }
    
    /**
     * 
     * @param key it's a string for accessing the map
     * @return call it is a string format that will print out to the gui window
     */
    public String getInfo(String key)
    {
    	String call = "";
    	AddressEntry info_List = addressEntryTable.get(key);
    	Address address = info_List.getAddress();
    	Name name = info_List.getName();
        call = info_List.toString(address, name);
		return call;
    }
    
    /**
     * 
     * @param key is a key to access to the map to get the entry
     * @return call is a string contain the entry of the contain
     */
    public String getInfoForCorrection(String key)
    {
    	String call = "";
    	AddressEntry info_List = addressEntryTable.get(key);
    	Address address = info_List.getAddress();
    	Name name = info_List.getName();
        call = info_List.toStringForCorrection(address, name);
		return call;
    }
    
    /**
     * 
     * @param key is string for accessing the map
     * @return info_List the entry of the contact info
     */
    public AddressEntry setInfo(String key)
    {
    	AddressEntry info_List = addressEntryTable.get(key);
		return info_List;
    }
    
    /**
     * 
     * @return ID the value store inside the array
     */
    public ArrayList<String> ID()
    {
		return ID;
    }
    
    /**
     * 
     * @return nums the value store inside the array
     */
    public ArrayList<String> getNumArray()
    {
		return nums;
    }
    
    /**
     * 
     * @return map_List this is a Map that contains all the contact. It will return to the AddressBookApplication
     */
    public Map<String,AddressEntry> getMap()
    {
    	// returning the map to the addressBookApplication.
        return (addressEntryTable);
    }
    
    /**
     * 
     * @param name is the slot of the array, that the key is store in.
     * From there I get the key to the Map, so I can remove the entry.
     */
    public void remove(int name)
    {
    	// when i got the number of the slot that the use pick, I call the array to get the key, and from there i call map to remove.
    	String remove = ID.get(name);
    	ID.remove(name);
    	AddressBookGUI gui = new AddressBookGUI();
    	gui.setRemoveKey(remove);
    }
    
    /**
     * clearing the num array for updating when searching
     */
    public void removeFromNum()
    {
    	nums.clear();
    }
    
    /**
     * 
     * @param name is the last name of the person
     * @return running_Total is the counting of how many entry there are in the Map
     * so this is the counting the name that matches in the Map
     */
    public int count(String find)
    {
		String name = find.toLowerCase();
    	int check = 0;
    	System.out.println(name);
    	char c = name.charAt(0);
    	if (find.length() > 1)
    	{
        	for (String key: addressEntryTable.keySet())
        	{
        		AddressEntry info_List = addressEntryTable.get(key);
        		Name lastName = info_List.getName();
            	if (find.equals(lastName.getLastName()))
            	{
            		// when it found the contact, it will store those name in the array.
            		nums.add(key);
            		check = 1;
            	}
        	}
    	}
        else
        {
        	for (String key: addressEntryTable.keySet())
        	{
        		AddressEntry info_List = addressEntryTable.get(key);
        		Name lastName = info_List.getName();
        		String lowerCase = lastName.getLastName().toLowerCase();
        		if (c == lowerCase.charAt(0))
        		{
        			nums.add(key);
        			check = 1;
        		}
        	}
        }
        return check;
    }

    /**
     * 
     * @return int for the size of the ID array
     */
    public int getSize()
    {
    	return ID.size();
    }
}
