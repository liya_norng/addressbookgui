import static org.junit.Assert.*;

import org.junit.Test;

public class AddressTest {

	@Test
	public void testSetStreet() 
	{
		Address contact = new Address();
		contact.setStreet("311 B Street");
		assertEquals("311 B Street", contact.getStreet());
		contact.setStreet("588 B Street");
		assertEquals("588 B Street", contact.getStreet());	
	}

	@Test
	public void testSetCity() 
	{
		Address contact = new Address();
		contact.setCity("Tracy");
		assertEquals("Tracy", contact.getCity());
		contact.setCity("Hayward");
		assertEquals("Hayward", contact.getCity());		
	}

	@Test
	public void testSetState() 
	{
		Address contact = new Address();
		contact.setState("Ca");
		assertEquals("Ca", contact.getState());
		contact.setState("Tx");
		assertEquals("Tx", contact.getState());	
	}

	@Test
	public void testSetZip() 
	{
		Address contact = new Address();
		contact.setZip("12345");
		assertEquals("12345", contact.getZip());
		contact.setZip("09876");
		assertEquals("09876", contact.getZip());	
	}
}
