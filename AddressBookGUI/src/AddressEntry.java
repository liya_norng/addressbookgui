
/**
 * @author Liya Norng
 * @date 2/17/16
 */

/**
 * this is the class AddressEntry.
 * @param Name an instance of class Name
 * @param Address an instance of class Address
 * @param email_Name is the email for storing the information.
 * @param phone_Name is the phone number for storing the information.
 * @param ID is the phone number for storing the information.
 */
public class AddressEntry {
    // Variables
    Name name = new Name();
    Address address = new Address();
    String email_Name;
    String phone_Name;
    String ID;
    
    
    /**
     * this is the constructor that accepts a first, last name, street, city,
     * state, zip, email, phone.
     */
    //constructor…one that accepts a first, last name, street, city, state, zip, email, phone.
    AddressEntry (String ID,String first_Name, String last_Name, String street_Name, String city_Name, String state_Name, String zip_Name, String email_Name, String phone_Name )
    {
    	
        name.setFirstName(first_Name);
        name.setLastName(last_Name);
        address.setCity(city_Name);
        address.setState(state_Name);
        address.setStreet(street_Name);
        address.setZip(zip_Name);
    	this.ID = ID;
        this.email_Name = email_Name;
        this.phone_Name = phone_Name;

    }
    
    /**
     * this is just a dummy constructor, which I need for one of the class, because the other constructor need attributes.
     */
    AddressEntry ()
    {
        // dummy constructor
    }
    
    /**
     * 
     * @return address a pointer to the class address
     * @return name a pointer to the class name
     */
    public Address getAddress()
    {
    	return address;
    }
    public Name getName()
    {
    	return name;
    }
    
    /**
     * @return string of contacts as a nicely format.
     */
    public String toStringForCorrection(Address address, Name name)
    {
    	Menu menu = new Menu();
        return (menu.FirstName() + "\n" + name.getFirstName() + "\n\n"+ menu.LastName() + "\n" + name.getLastName() + "\n\n" + menu.Street() + "\n" +  address.getStreet() + "\n\n" + menu.City() + "\n" +  address.getCity() + "\n\n" + menu.State() + "\n" + address.getState() + "\n\n" + menu.Zip() + "\n" + address.getZip() + "\n\n" + menu.Email() + "\n" + email_Name + "\n\n" + menu.Telephone() + "\n" + phone_Name);
    }
    public String toString(Address address, Name name)
    {
        return (name.getFirstName() + "		" + name.getLastName() + "		" + address.getStreet() + "		" +  address.getCity() + "		" + address.getState() + "		" + address.getZip() + "		" + email_Name + "		" + phone_Name);
    }
    public String toStringForSearch(Address address, Name name)
    {
        return (name.getFirstName() + "	" + name.getLastName() + "	" + address.getStreet() + "	" +  address.getCity() + "	" + address.getState() + "	" + address.getZip() + "	" + email_Name + "	" + phone_Name);
    }
    
    /**
     * all of this methods would set all the string to appropriate string.
     */
    public void setID(String ID)
    {
    	this.ID = ID;
    }
    public void setEmail(String email_Name)
    {
        this.email_Name = email_Name;
    }
    public void setPhone(String phone_Name)
    {
        this.phone_Name = phone_Name;
    }
    
    /**
     * all of this methods would return the string according to the methods name.
     * @return email_Name this value is to return to the request of the class.
     * @return phone_Name this value is to return to the request of the class.
     * @return ID this value is to return to the request of the class.
     */
    public String getID()
    {
    	return ID;
    }
    public String getEmail()
    {
        return email_Name;
    }
    public String getPhone()
    {
        return phone_Name;
    }
}
